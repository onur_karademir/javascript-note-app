
let newNoteBtn = document.querySelector(".new-note-add");

newNoteBtn.addEventListener("click",function(){
    const noteContainer = document.createElement("div");
    noteContainer.classList.add("note");
    noteContainer.innerHTML = `
    <div class="note-header">
            <button class="edit">edit</button>
            <button class="delete">delete</button>
    </div>
    <div class="text-container hide"></div>
    <textarea></textarea>
    `
    document.body.appendChild(noteContainer);

    let note = noteContainer.querySelector(".note");
    let editBtn = noteContainer.querySelector(".edit") ;
let deleteBtn = noteContainer.querySelector(".delete");

let textContainer = noteContainer.querySelector(".text-container");

let textArea = noteContainer.querySelector("textarea");

textArea.addEventListener("input",(e)=> {
    const {value} = e.target;
    textContainer.innerHTML = marked(value);
});

deleteBtn.addEventListener("click",function(){
    noteContainer.remove();
});

editBtn.addEventListener("click",function(){
    console.log("click");
    textContainer.classList.toggle("hide");
    textArea.classList.toggle("hide");
});
});